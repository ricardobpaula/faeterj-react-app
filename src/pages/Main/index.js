import React, { useEffect, useState } from 'react'

import { FaGithubAlt, FaPlus, FaSpinner } from 'react-icons/fa'
import { Link } from 'react-router-dom'

import api from '../../services/api'

import { Container, Form, SubmitButton, List } from './styles'

export default function Main(){

  const [newRepo, setNewRepo] = useState('')
  const [repositories, setRepositories] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const storageRepositories = localStorage.getItem('repositories')
    if(storageRepositories) {
      setRepositories(JSON.parse(storageRepositories));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('repositories', JSON.stringify(repositories))
  }, [repositories])

  function handleInputChange(e){
    setNewRepo(e.target.value)
  }

  async function handleSubmit(e){

    try {
      e.preventDefault()

      setLoading(true)
      setRepositories(repositories)
      setNewRepo(newRepo)

      const response = await api.get(`/repos/${newRepo}`)

      const data = {name: response.data.full_name}

      console.log(data.name)

      setRepositories([...repositories,data])
      console.log(repositories)
      setNewRepo('')
      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.log(error)
    }

  }

  return(
    <Container>
        <h1>
          <FaGithubAlt/>
          Repositórios
        </h1>

        <Form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Adicionar repositório"
            onChange={handleInputChange}
             />

          <SubmitButton
            loading={
              loading === 'true'
                ? Boolean(loading === 'true')
                : Boolean(loading === 'true')
            }
          >
            { loading ? (
              <FaSpinner color='#fff' size={14} />
            ) : (
              <FaPlus color='#fff' size={14} />
            )
            }
          </SubmitButton>
        </Form>

        <List>
          {repositories.map((repository)=> (
            <li key={repository.name}>
              <span>{repository.name}</span>
              <Link to={`/repository/${encodeURIComponent(repository.name)}`}>
                Detalhes
              </Link>
            </li>
          ))}
        </List>
    </Container>
  )
}
